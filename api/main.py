from fastapi import FastAPI, Depends, HTTPException
from sqlalchemy import select
from sqlmodel import Session
from typing import Union

from api.dal.db import init_db, get_session
from api.models import *

import api.services.category as category_service
import api.services.product as product_service

app = FastAPI()


@app.get("/api/categories", response_model=List[Category])
def get_categories(*, session: Session = Depends(get_session)):
    categories = category_service.get_categories(session)

    return categories

@app.get("/api/products", response_model=List[Product])
def get_products(*, session: Session = Depends(get_session), categoryId: Union[int, None] = None ):
    products = product_service.get_products(session, categoryId)

    return products

@app.post("/api/products", response_model=Product)
def create_product(*, session: Session = Depends(get_session), product: ProductCreate):
    added_prod = product_service.create_product(session, product) 

    return added_prod

@app.put("/api/products/{productId}", response_model=Product)
def update_product(*, session: Session = Depends(get_session), productId: int, product: ProductUpdate):
    updated_prod = product_service.update_product(session, productId, product) 

    return updated_prod

@app.patch("/api/products/{productId}", response_model=Product)
def patch_product(*, session: Session = Depends(get_session), productId: int, product: ProductUpdate):
    updated_prod = product_service.patch_product(session, productId, product) 

    return updated_prod

@app.get("/api/products/{productId}", response_model=Product)
def get_product(*, session: Session = Depends(get_session), productId: int):
    product = product_service.get_product(session, productId)

    return product

@app.delete("/api/products/{productId}")
def patch_product(*, session: Session = Depends(get_session), productId: int):
    result = product_service.delete_product(session, productId) 

    return result
