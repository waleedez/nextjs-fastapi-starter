import os

from sqlmodel import create_engine, SQLModel, Session

# DATABASE_URL = os.environ.get("")
DATABASE_URL = "postgresql://default:jdPY5OCMU8rz@ep-summer-tooth-341170.us-east-1.postgres.vercel-storage.com:5432/verceldb?options=endpoint=ep-summer-tooth-341170"
engine = create_engine(DATABASE_URL, echo=True)


def init_db():
    SQLModel.metadata.create_all(engine)


def get_session():
    with Session(engine) as session:
        try:
            yield session
        finally:
            session.close()
